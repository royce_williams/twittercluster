import twitter4j.TwitterException;

/**
 * Main class for project
 *
 * Control flow and handle errors
 */
public class TwitterCluster {
    public static void main(String args[]) {
        try  {
            TwitterReader tr = new TwitterReader();
            ClusterSentences cs = new ClusterSentences(TwitterReader.cleanTweets(tr.searchTweets(args[0] + " -filter:retweets", 1000)));
            if(!cs.outputResponseData(args[0])){
                System.out.println("Error outputting XML");
            }
            System.out.println("Cluster success: Results in results.xml");
        }
        catch(TwitterException tw){
            System.out.println(tw.getMessage());
        }
        catch(ArrayIndexOutOfBoundsException e){
            System.out.println("Usage: java -jar TweetCluster.jar <Twitter search term>");
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
}
