import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import twitter4j.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/**
 * Encapsulates access to the twitter4j api
 * Throws TwitterException
 */
public class TwitterReader {
    private Twitter unauthenticatedUser;

    /**
     * Default Constructor
     * - Initializes variables to defaults
     * @throws TwitterException
     */
    public TwitterReader() throws TwitterException {
        unauthenticatedUser = TwitterFactory.getSingleton();
    }

    // Map of rate limits from the twitter api
    // Key is a string related to the resource to be used.
    // Resource name is found in the twitter api call just after the version, for example:
    // https://api.twitter.com/1.1/media/upload.json - "media/upload" is the resource
    private Map<String, RateLimitStatus> rateLimitsMap;


    /**
     * Retrieves tweets made by user account
     * @param userName = username of public account
     * @param numTweets = int number of tweets to return, will round up by 200
     * @return List<Status> of tweets
     * @throws TwitterException
     */
    public List<Status> getTweets(String userName, int numTweets) throws TwitterException {
        List<Status> results;
        String apiCall = "/statuses/home_timeline";
        final int RES_PER_PAGE = 200;
        int queryTotal = (numTweets % RES_PER_PAGE > 0) ? numTweets / RES_PER_PAGE + 1 : numTweets / RES_PER_PAGE;
        rateLimitsMap = unauthenticatedUser.getRateLimitStatus();
        if(rateLimitsMap.get(apiCall).getRemaining() >= 3){
            results = unauthenticatedUser.getUserTimeline(userName, new Paging(1, RES_PER_PAGE));
            for(int page = 2; page <= queryTotal; page++) {
                results.addAll(unauthenticatedUser.getUserTimeline(userName, new Paging(page, RES_PER_PAGE)));
            }
        }
        else{
            throw new TwitterException("Exceeded rate limit for resource: [" + apiCall + "])");
        }
        return results;
    }

    /**
     * Searches twitter for specified searchTerm (or word)
     * @param searchTerm = search String
     * @param numTweets = int number of tweets to return, will round up by 100
     * @return List<Status> of tweets
     * @throws TwitterException
     */
    public ArrayList<Status> searchTweets(String searchTerm, int numTweets) throws TwitterException {
        ArrayList<Status> results = new ArrayList<>();
        String apiCall = "/search/tweets";
        final int RES_PER_PAGE = 100;
        int queryTotal = (numTweets % RES_PER_PAGE > 0) ? numTweets / RES_PER_PAGE + 1 : numTweets / RES_PER_PAGE;
        System.out.println("Requesting " + numTweets + " tweets using search " + searchTerm);

        //check rate limits to be sure there's enough left to get the required number of tweets
        rateLimitsMap = unauthenticatedUser.getRateLimitStatus();
        if(rateLimitsMap.get(apiCall).getRemaining() >= queryTotal){
            Query query = new Query(searchTerm);
            query.setCount(RES_PER_PAGE);
            long lastID = Long.MAX_VALUE;
            while (results.size () < numTweets) {
                if (numTweets - results.size() > 100)
                    query.setCount(100);
                else {
                    query.setCount(numTweets - results.size());
                }
                QueryResult qResult = unauthenticatedUser.search(query);
                results.addAll(qResult.getTweets());
                for (Status t: results) {
                    if (t.getId() < lastID) {
                        lastID = t.getId();
                    }
                }
                query.setMaxId(lastID-1);
            }
        }
        else{
            throw new TwitterException("Exceeded rate limit for resource: [" + apiCall + "])");
        }
        return results;
    }

    /**
     * Cleans tweets - removes urls, hashtags, usernames and non-word characters
     *              - removes any duplicate tweets
     * @param unclean = List<Status> of tweets to be cleaned
     * @return ArrayList<Status> of cleaned tweets
     * @throws TwitterException
     */
    public static ArrayList<Status> cleanTweets(List<Status> unclean) throws TwitterException{
        //Remove duplicate tweets
        HashSet<Status> seenStrings = new HashSet<>();
        System.out.println("Cleaning tweets");
        for(Status us : unclean){
            //remove urls, hashtags and usernames
            us = cleanStatus(us);

            //remove duplicate tweets not caught as retweets
            boolean add = true;
            for(Status seen : seenStrings){
                if(seen.getText().equals(us.getText())){
                    add = false;
                }
            }
            if(add){
                seenStrings.add(us);
            }
        }
        return new ArrayList<>(seenStrings);
    }

    private static Status cleanStatus(Status unclean) throws TwitterException {
        Gson gson = new Gson();
        String raw = gson.toJson(unclean);
        return TwitterObjectFactory.createStatus(cleanJSON(raw, "text"));
    }

    //Have to convert to json to edit the text field
    //No Status methods allow editing of a Status
    private static String cleanJSON(String input, String field){
        JsonObject json = new JsonParser().parse(input).getAsJsonObject();
        String raw = json.get(field).getAsString();

        raw = cleanString(raw);

        //Remove then replace field
        json.addProperty(field, raw);
        return json.toString();
    }

    public static String cleanString(String unclean){
        //Remove urls, courtesy of regexpal.com
        unclean = unclean.toLowerCase().replaceAll("(?i)(?:(?:https?|ftp|file)://|www\\.|ftp\\.)(?:\\([-A-Z0-9+&@#/%=~_|$?!:,.]*\\)|[-A-Z0-9+&@#/%=~_|$?!:,.])*(?:\\([-A-Z0-9+&@#/%=~_|$?!:,.]*\\)|[A-Z0-9+&@#/%=~_|$])", "");
        //Remove #, @, and excess symbols
        unclean = unclean.replaceAll("@|#|:", "").replaceAll("\\(|\\)", " ");
        return unclean.replaceAll("[^a-zA-Z\\d\\s\r\n\\r\\n:]", "").trim().replaceAll("\\s+", " ");
    }
}
