import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONObject;
import org.json.XML;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import twitter4j.Status;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class ClusterSentences{
    ArrayList<String> text = new ArrayList<>();
    HttpResponse<JsonNode> response;
    public ClusterSentences(ArrayList<Status> list){
        for(Status s : list){
            text.add(s.getText());
        }
    }

    private boolean postCluster(){
        System.out.println("Clustering tweets");
        try {
            response = Unirest.post("https://rxnlp-core.p.mashape.com/generateClusters")
                    .header("X-Mashape-Key", "jpKny53qaemshOXOI5VVnHCqLutRp1erkLujsnah8jymzkskj0")
                    .header("Content-Type", "application/json")
                    .header("Accept", "application/json")
                    .body(formatJson())
                    .asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private String formatJson(){
        String res = "{\n\"type\": \"pre-sentenced\",\n\"text\": [";
        for(int i = 0; i < text.size(); i++){
            if(i != 0){
                res += ",\n";
            }
            res += "{\"sentence\":\"" + text.get(i) + "\"}";
        }
        return res + "]\n}";
    }

    public String getResponseXML() throws Exception{
        if(response == null) {
            if (!postCluster()) {
                return "Error getting clusters";
            }
        }
        JSONObject json = new JSONObject(response.getBody());
        System.out.println("Writing XML to file");
        return prettyPrintXml(XML.toString(json.getJSONObject("object")));
    }

    private String prettyPrintXml(final String xml) throws Exception{
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(xml)));
        TransformerFactory tfact = TransformerFactory.newInstance();
        tfact.setAttribute("indent-number", new Integer(2));
        Transformer tf = tfact.newTransformer();
        tf.setOutputProperty(OutputKeys.INDENT, "yes");
        tf.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        Writer out = new StringWriter();
        tf.transform(new DOMSource(doc), new StreamResult(out));
        return out.toString();
    }

    public boolean outputResponseData(String fname){
        File file = new File(fname + "_results.xml");
        if(file.exists()){
            file.delete();
        }
        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8))){
            writer.write(getResponseXML());
        }
        catch(IOException e){
            e.printStackTrace();
            return false;
        }
        catch (Exception e){
            e.printStackTrace();
            return false;
        }
        return true;
    }
}